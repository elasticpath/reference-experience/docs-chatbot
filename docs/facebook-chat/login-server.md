---
id: login-server
title: Setting up the Login Server
sidebar_label: Setting up the Login Server
---

Set up and configure the Login Server for the Reference Chatbot (Facebook Messenger). The Login Server is the authentication bridge between the Reference Chatbot and the Facebook Messenger services.

## Configuration Parameter Descriptions

You must configure the following parameters in the `./src/ep.config.json` file:

| Parameter | Importance | Type | Description |
|---|---|---|---|
|`cortexApi.path`| Required| String| The URL, which is composed of the hostname and port, to access Cortex. By default, a web proxy is configured in the [Webpack](https://webpack.js.org/) configuration of the project. For local development, set this value to `/cortex` to redirect Cortex calls to the local proxy.|
|`cortexApi.scope`| Required| String| Name of the store from which Cortex retrieves data.|
|`cortexApi.pathForProxy`|Required|String| The path to which the [Webpack](https://webpack.js.org/) proxy routes the Cortex calls from the storefront. This value is a URL that consists of hostname and port of a running instance of Cortex. Leave this field blank to disable proxy.|

## Setting up a Development Environment

1. Ensure you have read through the documentation [here](./index.md)
2. Run the `cd login` command
3. To install dependencies, run the `npm install` command
4. Configure the `./src/ep.config.json` file as required for the environment.

    For more information, see the [Configuration Parameter Descriptions](#configuration-parameter-descriptions) section.

5. To start the server in development mode, run the `npm start` command
6. To see the running Reference Chatbot Login Page, go to `http://localhost:9000/`

## Setting up a Production Environment

1. Ensure you have read through the documentation [here](./index.md)
2. Run the `cd login` command
3. To install dependencies, run the `npm install` command
4. Configure the `./src/ep.config.json` file as required for the environment.

    For more information, see the [Configuration Parameter Descriptions](#configuration-parameter-descriptions) section.

5. To build the application in production mode, run the `npm run build` command.

    This Builds the application for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance. The build is minified and the filenames include the hashes.

6. You can host the built files on a web server of your choice.
7. To  the running Reference Chatbot Login Page, go to `http://localhost:9000/`.

The application contains a `Docker` folder that contains a Dockerfile, a Shell script, and an Nginx configuration file.

- The Dockerfile uses a NodeJS image and an Nginx image, and uses the Production environment. The Dockerfile contains two arguments (`SCOPE` and `CORTEX_URL`) that you can populate using the `--build-arg` argument when creating the Docker image.
- The Shell script populates the configuration file according to the arguments that are passed to the Docker image.
- The Nginx configuration exposes the built files that are created by the Dockerfile when running `npm run build`.
