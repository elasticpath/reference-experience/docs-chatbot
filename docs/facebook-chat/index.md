---
id: index
title: Reference Chatbot (Facebook Messenger)
sidebar_label: Overview
---

The Reference Chatbot (Facebook Messenger) is a flexible chatbot that integrates with Facebook Messenger and communicates with Elastic Path Commerce through the RESTful Cortex API. The Reference Chatbot uses Cortex API resources to access the commerce capabilities provided by Elastic Path Commerce.

![Chatbot Intro](assets/chatbot_intro.gif)

## Prerequisites

The instructions in this guide assume that you are familiar with the prerequisite technologies.

To install and customize the Reference Chatbot, you must have the following software installed:

- [Git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/en/download/)
- [Visual Studio Code](https://code.visualstudio.com/) with the following extensions:
    - [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
    - [ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- A valid Elastic Path development environment. For more information, see
[Getting Started](https://documentation.elasticpath.com/commerce/docs/getting-started/index.html) in the Elastic Path Commerce documentation.
- If you plan to implement the React PWA Reference Storefront, set up the storefront. For more information, see:
    - [React PWA Reference Storefront repository](https://github.com/elasticpath/react-pwa-reference-storefront/)
    - [React PWA Reference Storefront guide](https://documentation.elasticpath.com/storefront-react/docs/index.html)

## Setting up a Development Environment

Set up and configure the Reference Chatbot, and optionally integrate the Reference Chatbot with the React PWA (Progressive Web App) Reference Storefront.

1. Clone or pull the `facebook-chat` repository to your directory.
2. Run the `cd facebook-chat` command.
3. [Set up the Chatbot server](chatbot-server.html).
4. [Set up Login server](login-server.html).
5. (Optional) Integrate the Reference Chatbot with the React PWA Reference Storefront.
