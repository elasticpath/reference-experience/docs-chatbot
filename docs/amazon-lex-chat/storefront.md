---
id: storefront
title: Integrating with the React PWA Reference Storefront
sidebar_label: Integrating with Reference Storefront
---

If you are implementing the React PWA Reference Storefront as the basis for your storefront, you can follow this example to integrate the Reference Chatbot (Amazon Lex) with the storefront.

## Prerequisites

Ensure that you have completed the steps in the following sections:

1. [Setting up the Chatbot Server](setup.md)
2. [Setting up the Chatbot PWA Component](pwa.md).

## Integrate the Chatbot PWA Component with the Reference Storefront

The steps are written for the React PWA Reference Storefront. Your storefront implementation might be different.

1. In the `./react-pwa-reference-storefront/app/src/ep.config.json` file, update the `chatbot.enable` to **true**.
2. In the `./react-pwa-reference-storefront/app/src/ep.config.json` file, update the `chatbot.name` parameter with the name of the chatbot you have previously setup.
3. Update the `chatbot-welcome-msg` string in `./react-pwa-reference-storefront/app/localization/en-CA.json` file. By default, `en-CA` and `fr-FR` locales are provided in the project.
4. Go to the `react-pwa-reference-storefront/app` directory.
5. Run `amplify init`. You can accept all default values and configure if necessary.
6. Run `amplify add interactions` to create Lex Model hooks.
    1. Step through the creation wizard and choose **Use a Sample Chatbot**.
    2. Once the example bot is created, run `amplify push` to generate an `aws-exports.js` folder in your source directory.
7. In the following files, change the `BotName` field to the `EPConversationalBot` on your AWS account:
    * `./react-pwa-reference-storefront/app/src/aws-exports.js` - `aws_bots_config.name = "[Your Bot Name]"`
    * `./react-pwa-reference-storefront/app/src/App.js` - `botName = "[Your Bot Name]"`

After completing the setup, access your storefront and confirm that the Chatbot PWA component loaded upon logging into the store.

![Chatbot Storefront](assets/chatbot-storefront.png)
