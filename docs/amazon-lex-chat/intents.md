---
id: intents
title:  Elastic Path Intents Reference
sidebar_label:  Elastic Path Intents
---

The Reference Chatbot (Amazon Lex) includes predefined intents for Elastic Path Commerce. An intent is an expression of a shopper՚s intention. For example, the shopper might want to search for an item or add an item to a cart. An intent includes an action plus the sample phrases (called _utterances_) that can trigger the action. You can add additional utterances for the actions in the predefined intents. The intents are defined in the interaction model, which is defined in the [lex-model.json](https://github.com/elasticpath/lex-chatbot/blob/master/ep-lex-models/lex-model.json) file.

The following table contains a list of the predefined intents, organized by action:

| Action                    | Utterances                            | Intent Name                                           |
| ------------------------- | ----------------------------------------- | ----------------------------------------------------- |
| Initialize                    | `"Hello ep"`                       | `EPAuthIntent`                                 |
|                     | `"ep-auth {token}"`                       |                              |
|                     | `"{token}"`                       |                              |
| Search                    | `"Search for {item}"`                       | `KeywordSearchIntent`                                 |
|                     | `"Find {item}"`                       |                               |
|                     | `"What do you have for {item}"`                       |                               |
|                     | `"Do you carry {item}"`                       |                               |
|                     | `"Do you have {item}"`                       |                               |
|                     | `"Search for some {item}"`                       |                               |
|                     | `"Show me {item}"`                       |                               |
|                     | `"Find me {item}"`                       |                               |
| Next                      | `"Next item"`                               | `NextItemIntent`                                      |
|                       | `"Go forward"`                               |                                    |
|                       | `"Show me something else"`                               |                                      |
|                       | `"What else do you have"`                               |                                      |
|                       | `"Show me the next item"`                               |                                   |
|                       | `"What's next"`                               |                                   |
|                       | `"Next"`                               |                                   |
| Previous                  | `"Previous item"`                           | `PreviousItemIntent`                                  |
|                  | `"Show me the last item"`                           |                                 |
|                  | `"What was the last item"`                           |                                 |
|                  | `"Previous"`                           |                               |
|                  | `"Go back"`                           |                               |
|                  | `"Last"`                           |                               |
| Show Specific Item          | `"What's item ​{itemNo}​"`                               | `ShowListedItemIntent`                  |
|                       | `"What was ​{itemNo}​"`                               |                                    |
|                       | `"What was item ​{itemNo}​"`                               |                                      |
|                       | `"What was item number ​{itemNo}​"`                               |                                      |
|                       | `"Go to ​{itemNo}​"`                               |                                   |
|                       | `"Go to item ​{itemNo}"`                               |                                   |
|                       | `"Show me item ​{itemNo}​"`                               |                                   |
|                       | `"Show me item number ​{itemNo}​​"`                               |                                   |
| Describe Current Product  | `"Tell me more about that"`                 | `DescribeProductIntent`                               |
|               | `"show me"`                 |                       |
|               | `"what is that"`                 |                       |
|               | `"what is this"`                 |                       |
|               | `"what's that"`                 |                       |
|               | `"describe it"`                 |                       |
|               | `"describe this item"`                 |                       |
|               | `"tell me more about the item"`                 |                       |
|               | `"i want to know more about it"`                 |                       |
|               | `"i want to hear more"`                 |                       |
|               | `"tell me about it"`                 |                       |
|               | `"tell me about them"`                 |                       |
|               | `"give me more info about the product"`                 |                       |
| Add to Cart               | `"Add that to my cart"`                     | `AddToCartIntent`                                     |
|              | `add ​{amount}​ to cart"`                     |                                   |
|              | `i'd like ​{amount}​ of those"`                     |                                   |
|              | `add ​{amount}​ of those to my cart"`                     |                                   |
|              | `Add that item to my cart."`                     |                                   |
|              | `Add it to cart."`                     |                                   |
|              | `Put ​{amount}​ in the cart."`                     |                                   |
|              | `Add the item to my cart."`                     |                                   |
|              | `Put ​{amount}​ in my cart."`                     |                                   |
|              | `Put it in my cart."`                     |                                   |
| Explore Cart              | `"What’s in my cart?"`                      | `GetCartIntent`                                       |
|             | `"See shopping cart"`                      |                                 |
|             | `"Check cart"`                      |                                 |
|             | `"Cart"`                      |                                 |
|             | `"Go to cart."`                      |                                 |
|             | `"Go to my cart."`                      |                                 |
|             | `"Go to the cart."`                      |                                 |
| Remove from Cart          | `"Remove this from my cart"`                | `RemoveFromCartIntent`                                |
|        | `"remove"`                |                             |
|        | `"remove that"`                |                             |
|        | `"remove from cart"`                |                             |
|        | `"remove from my cart"`                |                             |
|        | `"remove this item from my cart"`                |                             |
| Checkout                  | `"I’d like to check out"`                   | `CheckOutIntent`                                      |
|                   | `"checkout my cart"`                   |                                  |
|                   | `"checkout"`                   |                                  |
