---
id: index
title: Reference Chatbot (Amazon Lex)
sidebar_label: Overview
---

Integrate a conversational interface with the Elastic Path Commerce platform by implementing this reference experience of an Amazon Lex chatbot. The Reference Chatbot (Amazon Lex) allows flexible deployment to a variety of conversational interfaces, including Facebook Messenger and Slack. The Reference Chatbot communicates with the Elastic Path Cortex API to leverage the commerce capabilities provided by Elastic Path Commerce. Optionally, you can integrate the Reference Chatbot with the React PWA (Progressive Web App) Reference Storefront.

![Architecture Diagram](assets/architecture.png)

## Assumptions

The instructions in this guide assume that you are familiar with the following technologies:

- [Git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/en/download/)
- [Amazon DynamoDB](https://aws.amazon.com/dynamodb/)
- [AWS Lambda Functions](https://aws.amazon.com/lambda/)
- [Amazon Lex Models](https://aws.amazon.com/lex/)

## Prerequisites

To install and customize the Reference Chatbot, you must have some accounts set up and the required software installed.

### Accounts

- A valid [Amazon Web Services (AWS) account](https://us-west-2.console.aws.amazon.com/console/)
- A valid [Amazon Developer account](https://developer.amazon.com/)

### Software

- [Git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/en/download/)
- [Amplify Command Line Interface (CLI)](https://aws-amplify.github.io/docs/)
- A valid Elastic Path Commerce development environment.
For more information, see
[Getting Started](https://documentation.elasticpath.com/commerce/docs/getting-started/index.html) in the Elastic Path Commerce documentation.
- If you plan to integrate the chatbot with the React PWA Reference Storefront, set up the storefront.
    - [React PWA Reference Storefront repository](https://github.com/elasticpath/react-pwa-reference-storefront/)
    - [React PWA Reference Storefront guide](https://documentation.elasticpath.com/storefront-react/docs/index.html)

## Project Structure

The `github.com/elasticpath/lex-chatbot` repository has the following subdirectories:

- `ep-lambda-function` - contains the AWS Lambda Deployment package
- `ep-lex-models` - contains the Amazon Lex Model
- `ep-pwa-chatbot` - contains the PWA chatbot component

For more information about the Lex Model, see the [Amazon Lex Developer Documentation](https://docs.aws.amazon.com/lex/latest/dg/import-export.html).
