---
id: pwa
title: Setting up the Chatbot PWA Component
sidebar_label: Setting up the Chatbot PWA Component
---

You can integrate the Reference Chatbot (Amazon Lex) with a front-end chat interface, such as the Chatbot PWA. The Chatbot PWA makes use of the [Amazon Amplify](https://aws-amplify.github.io/docs/) chatbot component as the front-end to the Lex Conversational service. The Chatbot PWA component can be used in a variety of front-end web applications, and is used in the React PWA Reference Storefront.

Perform the following tasks:

1. [Start the Chatbot PWA](#start-the-chatbot-pwa)
2. [Deploy the Chatbot PWA](#deploy-the-chatbot-pwa)
3. [Use the Chatbot PWA](#use-the-chatbot-pwa)

## Prerequisites

Before you begin, ensure that you have met the following prerequisites:

1. Create an AWS account where you will deploy the Chatbot PWA.
2. Create an AWS Identity and Access Management (IAM) profile.
3. Install the AWS Command Line Interface (CLI) tool.
For more information about platform specific procedures, see the [Amazon documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html).
4. Set up your AWS credentials on the AWS CLI.
    **Note:** Open a new terminal, and run `aws configure`.
5. Install [AWS Amplify CLI](https://aws-amplify.github.io/docs/).
6. Complete the instructions in [Setting up the Chatbot Server](setup.html).

## Start the Chatbot PWA

The following example shows how go to the `ep-pwa-chatbot` directory, install dependencies, and start the application.

```bash
# Go into the cloned directory
cd lex-chatbot/ep-pwa-chatbot

# Install all the dependencies for the project
npm install

# Start the app in development mode

# Run the main application:
npm start
```

## Deploy the Chatbot PWA

Configure the Chatbot PWA through the Amplify CLI.

The following steps walk through a sample deployment of the Chatbot PWA with Amazon Amplify.

1. Go to the `lex-chatbot/ep-pwa-chatbot` directory.
2. Run `amplify init`. You can accept all default values and configure if necessary.
3. Run `amplify add hosting` to enable static web hosting for the app on Amazon S3.
4. Run `amplify add interactions` to create Lex Model hooks.
   1. Step through the creation wizard and choose **Use a Sample Chatbot**.
   2. After the example chatbot is created, run `amplify push` to generate an `aws-exports.js` folder in your source directory.
5. In the following files, change the `BotName` field to the `EPConversationalBot` on your AWS account:
   - `./ep-pwa-chatbot/src/aws-exports.js` - `aws_bots_config.name = "[Your Bot Name]"`
   - `./ep-pwa-chatbot/src/App.js` - `botName = "[Your Bot Name]"`
6. Run `amplify publish` to build your Chatbot PWA, and invoke the push to Amazon S3.
7. Access your Chatbot PWA at the domain name provided in the console.

![Chatbot PWA](assets/chatbot-pwa.png)

## Use the Chatbot PWA

The Chatbot PWA operates under the following shopper types: public user and registered user. Follow the instructions for either the public user or registered user, not both.

### Public User

Request a public user token to enable the chatbot to interact with Cortex.

- In the Chatbot PWA window, type `Hello ep` to initialize the chatbot component.

### Registered User

The Chatbot Server accepts a Cortex bearer token passed through the intent as a slot under the type `AuthToken` with name `token`. The token is set as a session attribute, and is propagated to the lambda to perform actions on behalf of the authenticated user. This token is intended to be passed through the intent and then served as a session attribute through the Amazon Amplify framework.

- Invoke the `ep-auth {token}` utterance, where `token` is the slot for the bearer token.
