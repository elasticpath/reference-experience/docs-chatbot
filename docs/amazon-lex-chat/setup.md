---
id: setup
title: Setting up the Chatbot Server
sidebar_label: Setting up the Chatbot Server
---

To avoid unnecessary complications, perform the steps in the following order:

1. [Clone the repository](#clone-the-repository)
2. [Set up an Amazon DynamoDB instance](#set-up-an-amazon-dynamodb-instance)
3. [Set up the AWS Lambda function](#set-up-the-aws-lambda-function)
4. [Link the Lambda function to DynamoDB](#link-the-lambda-function-to-dynamodb)
5. [Set up the Amazon Lex Model](#set-up-the-amazon-lex-model)

## Clone the Repository

The following example shows how to clone the `github.com/elasticpath/lex-chatbot` repository and install the project dependencies.

```bash
# Clone the Git repository
git clone https://github.com/elasticpath/lex-chatbot.git

# Go into the cloned directory
cd lex-chatbot/ep-lambda-function

# Install all the dependencies for the project
npm install
```

## Set up an Amazon DynamoDB Instance

The Reference Chatbot uses a simple Amazon-hosted noSQL database instance to track purchase state across multiple active devices.

1. In the AWS console, go to the DynamoDB service.
2. Create a new table.
3. Set the name to `DynamoCache`.
4. Set the primary key to `responseId`.
5. In the `Table Overview`, locate the `Time to Live Attribute` and click `Enable TTL`.
6. Set the `TTL attribute` to `ttl` and click **Continue**.

## Set up the AWS Lambda Function

You must have a valid Elastic Path Commerce platform. The platform is used for the backend of the lambda.

1. Zip up the contents of the current directory, including the `node_modules` folder.
2. In the Amazon Lambda Console, create a new Function called `EPConversationalLambda`.
3. Select **Author From Scratch** template.
4. Choose `Node.js 10.x` as the runtime language.
5. Under the **Function Code** pane, change **Code Entry Type** to `Upload a .zip folder`.
6. Locate and upload the newly zipped folder.
7. Under **Environment Variables**, add and set the following values then save the function:

| Key            | Value                                |
| -------------- | ------------------------------------ |
| `CACHE_TABLE`    | DynamoCache                          |
| `CACHE_REGION`    | `<Your_AWS_Region>`                          |
| `SCOPE`          | `<Your_Cortex_Scope>`                               |
| `CORTEX_URL`     | `<Your_Cortex_URL>`  |
| `HOST_URL`     | `<Your_Host_Service_URL>`  |
| `SKU_IMAGES_URL`     | `<Your_SKU_Images_URL>`  |
| `ROLE`           | PUBLIC                           |
| `GRANT_TYPE`     | password                             |
| `USERNAME`       | [empty]           |
| `PASSWORD`       | [empty]                         |

## Link the Lambda Function to DynamoDB

Link the lambda function to DynamoDB by using IAM (Identity and Access Management) to read and write to the database table. This action preserves the state of the transaction.

1. In the AWS Lambda Designer, click the **Key** icon to view Permissions.
2. In the Execution Role pane, find the `roleName` value. Copy the value to the clipboard.
3. Browse Amazon Services, and search for IAM (Identity and Access Management).
4. In the IAM Console, go to the **AWS Account** field, select `Roles`.
5. In the search bar, paste the `roleName` and select the `EPConversationalLambda` role.
6. In the `Permissions` tab, select the `AWSLambdaBasicExecutionRole`.
7. Click `Edit Policy`.
8. Click `Add additional permissions` and select DynamoDB as the service.
9. Under the **Actions** drop-down list, enable Access level for `List`, `Read`, and `Write`.
10. Under the **Resources** drop-down list, enable All resources.
11. Click **Review Policy**.
12. Ensure the DynamoDB Access level is accurate.
13. Click to save your changes.

## Set up the Amazon Lex Model

Import the Lex Model into Amazon Lex and then set the fulfillment logic.

1. Go to the `lex-chatbot/ep-lex-models` directory.
2. Add the `lex-model.json` file to a zip folder.
3. Go to the Amazon Lex console to view your bots.
   **Note**: You must have at least one bot available to import an existing model. Create one of the pre-existing bots as a sample to access this feature.
4. Under the **Actions** drop-down list, select `Import`.
5. Upload the `lex-model.zip` file.
6. Set the Fulfillment logic to `AWS Lambda function`, and select your function from the drop-down list. Save the Intent.
7. Repeat step 6 for each Intent.
