---
id: lex-cards
title: Displaying Product Images in Response Cards
sidebar_label: Displaying Product Images
---

You can configure the Reference Chatbot lambda function to return product images as attachments for the response cards in the response from Amazon Lex. These images can be used with front-end touchpoints that can render response cards directly within the chat interface, such as Facebook, Slack, or Twilio. For more information about using response cards in Amazon Lex, see the [Amazon documentation](https://docs.aws.amazon.com/lex/latest/dg/ex-resp-card.html).

## Prerequisites

Before you begin, ensure that you have met the following prerequisites:

1. Create images for your products, where the filename of the image matches the sku number of the item.
2. Upload your catalog images to a Content Delivery Network (CDN) of your choice.
3. Complete the instructions in [Setting up the Chatbot Server](setup.md).

## Configure the `EPConversationalLambda` Function

Update the lambda function to set the `SKU_IMAGES_URL` property. Set this property to the fully-qualified URL for the directory that contains your hosted images. The lambda function will append the `<item-sku>.png` fetched from Cortex onto the end this URL.

1. In the Amazon Lambda Console, locate the `EPConversationalLambda` function.
2. In the **Environment Variables** field, configure the `SKU_IMAGES_URL` property and save the function.

   For example: `SKU_IMAGES_URL` = `https://s3-us-west-2.amazonaws.com/ep-demo-images/VESTRI_VIRTUAL`

The responses from Amazon Lex pertaining to product lookups return `responseCard` elements that include URLs to the relate product images. For example, the following code sample shows a `responseCard` for a product called "Mad Max Bundle" that includes a product image located at `https://s3-us-west-2.amazonaws.com/elasticpath-demo-images/VESTRI_VIRTUAL_TMP/78547.png`:

```json
{
  "dialogState": "Fulfilled",
  "intentName": "KeywordSearchIntent",
  "message": "Okay. I found 1 result for mad max. The first result is: \"Mad Max Bundle\"",
  "messageFormat": "PlainText",
  "responseCard": {
    "contentType": "application/vnd.amazonaws.card.generic",
    "genericAttachments": [
      {
        "attachmentLinkUrl": null,
        "buttons": [
          {
            "text": "Add to cart",
            "value": "Add it to my cart"
          }
        ],
        "imageUrl": "https://s3-us-west-2.amazonaws.com/elasticpath-demo-images/VESTRI_VIRTUAL_TMP/78547.png",
        "subTitle": "$754.80",
        "title": "Mad Max Bundle"
      }
    ],
    "version": "1"
  },
```

## Test a Response with Images

You can test the chatbot responses in the Amazon Lex test interface. This interface is capable of rendering the response cards with the URLs to the product images. You can interact with the response cards to perform actions.

![Chatbot Response Cards](assets/chatbot-response-cards.png)
