---
id: index
title: Reference Chatbot Quick Start Guide
sidebar_label: Introduction
---

A chatbot is a conversational interface that simulates a conversation with a user. In eCommerce, a shopper can use a chatbot to ask questions or perform actions while visiting a store. The chatbot takes keywords from the shopper՚s input, searches for the correct action, and responds to the shopper. For example, if a shopper asks about a product, the chatbot might respond with the product description. If a shopper then asks to add the item to a cart, the chatbot adds the item. The more effort you put into defining the chatbot behavior, the more robust the chatbot՚s ability to respond correctly to shoppers.

Elastic Path provides Reference Chatbot implementations for the following technologies:

- [Amazon Lex](amazon-lex-chat/index.md)
- [Facebook Messenger](facebook-chat/index.md)

The React PWA Reference Storefront implements the Reference Chatbot (Amazon Lex). Previous versions of the Reference Storefront implemented the Reference Chatbot (Facebook Messenger).

**Note**: Reference Chatbot (Facebook Messenger) is supported for the specified versions only. The instructions are no longer updated.
